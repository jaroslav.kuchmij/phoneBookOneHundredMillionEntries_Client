package main

import (
	"log"
	"google.golang.org/grpc"
	"os"
	"fmt"
	"context"
	"gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Client.git/StreamOneContactsFromManyFiles/rrs"
	"gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Client.git/StreamOneContactsFromManyFiles/model"
	"encoding/csv"
	"io"
	"github.com/spf13/viper"
)

var (
	address string // адрес и порт для TCP
	quantityCSV int // количество файлов
	pathTOTheFile string // путь к  csv файлу
)

func main()  {
	//получение данных из конфиг файла
	err := getConfig()
	if err != nil {
		log.Fatalf("failed to getConfig: %v", err)
	}
	//создание подключение к серверу
	err, client, conn := connectingServer()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	//передача данных на сервер
	err = streemData(client)
	if err != nil {
		panic(err)
	}
}

func connectingServer() (error, rrs.PhoneBookClient, *grpc.ClientConn) {
	//Создание канала для связи с сервером
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return err, nil, nil
	}
	//Клиентская заглушка
	client := rrs.NewPhoneBookClient(conn)
	return err, client, conn
}

func streemData(client rrs.PhoneBookClient) error {
	var count = 1 //указатель на файл
	for count <= quantityCSV  {
		//генерация имени файла
		var nameCSV = fmt.Sprintf(pathTOTheFile + "%d.csv", count)
		//открытие файла
		csvFile, err := os.Open(nameCSV)
		if err != nil {
			return err
		}
		//создание Reader
		reader := csv.NewReader(csvFile)
		reader.FieldsPerRecord = -1
		//Создание стрима
		stream, err := client.SetAllContactsInBolt(context.Background())
		if err != nil {
			return err
		}
		for {
			//чтение файла
			record, err := reader.Read()
			if err == io.EOF {
				break
			}
			//формирование запроса
			request := &rrs.ContactRequest{
				Contact: &model.Contact{
					Number: record[0],
					Name: record[1],
					Address: &model.Contact_Address{
						Street: record[2],
						NumberHouse: record[3],
						NumberFlat: record[4],
					},
				},
			}
			//передача данных на сервер
			if err := stream.Send(request); err != nil{
				return err
			}
		}
		//закрытие канала
		reply, err := stream.CloseAndRecv()
		if err != nil{
			return err
		}
		log.Printf("Reply: %d", reply)
		count++
	}
	return nil
}

// Получение данных из сонфигурационного файла
func getConfig() error {
	//название конфиг файла
	viper.SetConfigName("config")
	//путь к конф файлу
	viper.AddConfigPath("settings")

	//читаем конф файл
	if err := viper.ReadInConfig(); err != nil {
		return err
	//получение настроек из конф файла и запись в локальные переменные
	} else {
		address = viper.GetString("server.address")
		quantityCSV = viper.GetInt("server.quantityCSV")
		pathTOTheFile = viper.GetString("pathTOTheFile")
	}
	return nil
}