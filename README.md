# Project "Phonebook. One hundred million entries. (Client)"

## Важно! 
Для удачного запуска проекта, необходимо запустить сервер из проекта:
https://gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Server.git

### Запуск проекта:
1. Установка необходимых инструментов:
    - установка ProtoBuf. В терминале:
    <pre>
    go get github.com/golang/protobuf/protoc-gen-go
    </pre>
    Подробо о том, как создать проект c использованием ProtoBuf можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf)
    - установка BoltDB. В терминале
    <pre>
    go get github.com/boltdb/bolt/...
    </pre>
    - установк gRPC. В терминале
    <pre>
    go get google.golang.org/grpc
    </pre>
    
2. Скачать проект 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Client.git
</pre>

#### Запуск StreamOneContactsFromManyFiles
1. Скачать файл CSV с данными (100 миллионов записей): https://gobook.io/lunny/gitea/commit/663b5604ceaaf785ea45c57f2432d83f2329a302.
    ВАЖНО! в %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Client.git/StreamOneContactsFromManyFiles/settings
    находится путь к файлу CSV, который нужно задать

2. Перейти в пакет:
%GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Client.git/StreamOneContactsFromManyFiles/settings
и указать настройки для генерации CSV файла

3. Перейти в пакет: 
%GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Client.git/StreamOneContactsFromManyFiles
и запустить команду:
<pre>
go run main.go
</pre>


**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)